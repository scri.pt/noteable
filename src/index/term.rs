
use std::fmt;
use std::str;
use byteorder::{ByteOrder, BigEndian, LittleEndian};

/// Limited forward compatibility
const MAX_SAFE_VERSION: u8 = 8;

/// A term can have an error
#[derive(Debug)]
pub struct TermError {
    reason: String
}

/// Term error object
impl TermError {
    pub fn new(reason: &str) -> Self {
        TermError { reason: reason.to_string() }
    }
    pub fn reason(&self) -> &str {
        &self.reason
    }
}

/// Term field positions
#[derive(Debug, Copy, Clone)]
pub enum TermField {                /* back */
    Version = 1,                    /* +u8  */
    Strlen = 3,                     /* +u16 */    /*  -- -- - flags - -- -- */
    Flags = 5,                      /* +u16 *  ->  *  10 00 0000 0000 10 01 */
    Attributes = 6,                 /* +u16 */    /*  | id: Varint ___/  /  */
    VariableBegin = 7,              /* +u16 */    /*  | ord: Varint ____/   */
    MinSize = 8,                    /* mark */    /*  \___ is_padded: bool  */
    /* fixed-size fields */         /* .... */    /*  -- -- - ----- - -- -- */
    /* potential padding */         /* ~u16 */
    /* variable-length string */    /* .... */          
                                    /* front */
}

/// Term flag bitmasks
pub enum TermFlags {
    IdentifierMask = 0b0011,
    OrdinalMask = 0b1100,
    Padded = 0b1000_0000
}

/// Variable-size integer type
#[derive(Debug, Copy, Clone)]
pub enum Varint {   /* as usize */
    Tiny = 1,       /* u8 */
    Small = 2,      /* u16 */
    Medium = 4,     /* u32 */
    Large = 8       /* u64 */
}

/// Variable-size integer implementation
impl Varint {
    pub fn from_flags(flags: u16) -> Varint {
        debug_assert!(flags < 4);
        match flags {
            0 => Varint::Tiny,
            1 => Varint::Small,
            2 => Varint::Medium,
            3 => Varint::Large,
            _ => unreachable!("Unsupported identifier size")
        }
    }

    pub fn to_flags(&self) -> u16 {
        match self {
            Varint::Tiny => 0,
            Varint::Small => 1,
            Varint::Medium => 2,
            Varint::Large => 3
        }
    }

    pub fn from_u64(n: u64) -> Varint {
        match n {
            0x0..=0xff /* 8 */ => Varint::Tiny,
            0x100..=0xffff /* 16 */ => Varint::Small,
            0x1_0000..=0xffff_ffff /* 32 */ => Varint::Medium,
            0x1_0000_0000..=0xffff_ffff_ffff_ffff /* 64 */ => Varint::Large
        }
    }

    fn write(buf: &mut [u8], n: u64) -> Varint {
        let rv = Varint::from_u64(n);
        match rv {
            Varint::Tiny =>
                buf[0] = n as u8,
            Varint::Small =>
              BigEndian::write_u16(&mut buf[..], n as u16),
            Varint::Medium =>
              BigEndian::write_u32(&mut buf[..], n as u32),
            Varint::Large =>
              BigEndian::write_u64(&mut buf[..], n)
        }
        rv
    }

    fn read(&self, buf: &[u8]) -> u64 {
        match self {
            Varint::Tiny =>
              buf[0] as u64,
            Varint::Small =>
              BigEndian::read_u16(&buf[..]) as u64,
            Varint::Medium =>
              BigEndian::read_u32(&buf[..]) as u64,
            Varint::Large =>
              BigEndian::read_u64(&buf[..])
        }
    }
}

/// An index term
pub struct Term<'t> {
    buffer: &'t [u8],
    actual_size: usize,
    expected_size: usize
}

/// A mutable index term
pub struct TermMut<'t> {
    buffer: &'t mut [u8],
    actual_size: usize,
    expected_size: usize
}

/// A term has a buffer with a size
pub trait TermBase<'t> {
    fn to_buffer(&self) -> &[u8];
    fn actual_size(&self) -> usize;
    fn expected_size(&self) -> usize;
    fn constrain_size(&mut self, s: usize);
}

/// Shared between `Term` and `TermMut`
macro_rules! term_impl_base {
    ($struct:ident) => {
        impl<'t> TermBase<'t> for $struct<'t> {
            fn to_buffer(&self) -> &[u8] { &self.buffer }
            fn actual_size(&self) -> usize { self.actual_size }
            fn expected_size(&self) -> usize { self.expected_size }
            fn constrain_size(&mut self, s: usize) {
                debug_assert!(s <= self.buffer.len());
                self.actual_size = s;
            }
        }
    }
}

// Read-only version
term_impl_base!(Term);

// Read-write version
term_impl_base!(TermMut);

/// Mutable underlying buffer, read/write access
impl<'t> TermMut<'t> {
    fn to_buffer_mut(&mut self) -> &mut [u8] {
        &mut self.buffer
    }
}

/// Raw read-only access to term data
trait TermRaw<'t> {
    fn get_u8(&self, field: TermField) -> u8;
    fn get_u16(&self, field: TermField) -> u16;
    fn get_u32(&self, field: TermField) -> u32;
    fn get_u64(&self, field: TermField) -> u64;
    fn get_varint(&self, kind: Varint, delta: usize) -> u64;
}

/// Raw read-write access to term data
trait TermRawMut<'t> {
    fn write_u8(&mut self, field: TermField, n: u8);
    fn write_u16(&mut self, field: TermField, n: u16);
    fn write_u32(&mut self, field: TermField, n: u32);
    fn write_u64(&mut self, field: TermField, n: u64);
}

/// Structured access to term data
pub trait TermReadable<'t> {
    fn size(&self) -> usize;
    fn string(&self) -> &str;
    fn strlen(&self) -> usize;
    fn flags(&self) -> u16;
    fn version(&self) -> u8;
    fn attributes(&self) -> u8;
    fn ordinal(&self) -> u64;
    fn ordinal_size(&self) -> Varint;
    fn identifier(&self) -> u64;
    fn identifier_size(&self) -> Varint;
    fn validate(&mut self) -> Result<usize, TermError>;
    fn display(&self, name: &str, f: &mut fmt::Formatter<'_>) -> fmt::Result;
}

/// Structured modification of term data
pub trait TermWritable<'t> {
    fn write(&mut self, s: &str, id: u64, ord: u64)
             -> Result<usize, TermError>;
}

/// Term read primitives
impl<'t, T> TermRaw<'t> for T
where
    T: TermBase<'t>
{
    fn get_u8(&self, field: TermField) -> u8 {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer();
        buf[offset]
    }

    fn get_u16(&self, field: TermField) -> u16 {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer();
        LittleEndian::read_u16(&buf[offset..])
    }

    fn get_u32(&self, field: TermField) -> u32 {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer();
        LittleEndian::read_u32(&buf[offset..])
    }

    fn get_u64(&self, field: TermField) -> u64 {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer();
        LittleEndian::read_u64(&buf[offset..])
    }

    fn get_varint(&self, kind: Varint, delta: usize) -> u64 {
        let offset = self.actual_size() - delta
          - (kind as usize) - (TermField::VariableBegin as usize - 1);

        let buf = self.to_buffer();
        kind.read(&buf[offset..])
    }
}

/// Term write primitives
impl<'t> TermRawMut<'t> for TermMut<'t> {
    fn write_u8(&mut self, field: TermField, n: u8) {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer_mut();
        buf[offset] = n;
    }

    fn write_u16(&mut self, field: TermField, n: u16) {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer_mut();
        LittleEndian::write_u16(&mut buf[offset..], n)
    }

    fn write_u32(&mut self, field: TermField, n: u32) {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer_mut();
        LittleEndian::write_u32(&mut buf[offset..], n)
    }

    fn write_u64(&mut self, field: TermField, n: u64) {
        let offset = self.actual_size() - field as usize;
        let buf = self.to_buffer_mut();
        LittleEndian::write_u64(&mut buf[offset..], n)
    }
}

/// Structured term reading and validation
impl<'t, T> TermReadable<'t> for T
where
    T: TermBase<'t> /* For both `Term` and `TermMut` */
{
    fn validate(&mut self) -> Result<usize, TermError> {
        let buf = self.to_buffer();

        /* Check if it's safe to read fixed fields */
        if buf.len() < TermField::MinSize as usize {
            return Err(TermError::new("Term is impossibly small"));
        }

        /* Safe access */
        let flags = self.flags();
        let strlen = self.strlen();
        let version = self.version();

        /* Check version: forward and backward compatible */
        if version > MAX_SAFE_VERSION {
            return Err(TermError::new("Term has incorrect version"));
        }

        /* Calculate anticipated size (may be smaller than slice) */
        let mut expected_size = Term::calculate_size(
          strlen, self.identifier_size(), self.ordinal_size()
        );

        /* Check and adjust for padding, if present */
        if flags & TermFlags::Padded as u16 != 0 {
            if buf[strlen] != 0 && buf[strlen] != 0xff {
                return Err(TermError::new("Term padding is corrupted"));
            }
            expected_size += 1;
        }

        /* Final size check */
        if buf.len() < expected_size {
            return Err(TermError::new("Term is too small"));
        }

        /* UTF-8 validation occurs here once, and only once */
        if let Err(_e) = str::from_utf8(&buf[..strlen]) {
            return Err(TermError::new("Term contains invalid UTF-8"));
        }

        Ok(expected_size)
    }

    fn string(&self) -> &str {
        unsafe {
          /* This was already checked in `validate`, at construction */
          return str::from_utf8_unchecked(&self.to_buffer()[0..self.strlen()]);
        }
    }

    fn size(&self) -> usize {
        self.actual_size()
    }

    fn strlen(&self) -> usize {
        self.get_u16(TermField::Strlen) as usize
    }

    fn flags(&self) -> u16 {
        self.get_u16(TermField::Flags)
    }

    fn attributes(&self) -> u8 {
        self.get_u8(TermField::Attributes)
    }

    fn version(&self) -> u8 {
        self.get_u8(TermField::Version)
    }

    fn identifier_size(&self) -> Varint {
        Varint::from_flags(self.flags() & TermFlags::IdentifierMask as u16)
    }

    fn identifier(&self) -> u64 {
        self.get_varint(self.identifier_size(), self.ordinal_size() as usize)
    }

    fn ordinal_size(&self) -> Varint {
        Varint::from_flags((self.flags() & TermFlags::OrdinalMask as u16) >> 2)
    }

    fn ordinal(&self) -> u64 {
        self.get_varint(self.ordinal_size(), 0)
    }

    fn display(&self, name: &str, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(name)
          .field("string", &self.string())
          .field("id", &self.identifier())
          .field("attrs", &format!("{:b}", &self.attributes()))
          .field("isize", &self.identifier_size())
          .field("ordinal", &self.ordinal())
          .field("osize", &self.ordinal_size())
          .field("flags", &format!("{:b}", self.flags()))
          .field("strlen", &self.strlen())
          .field("bytes", &self.size())
          .finish()
    }
}

/// Debug display: Term
impl<'t> fmt::Debug for Term<'t> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.display("Term", f)
    }
}

/// Debug display: TermMut
impl<'t> fmt::Debug for TermMut<'t> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.display("TermMut", f)
    }
}

/// Term writing and creation
impl<'t> TermWritable<'t> for TermMut<'t> {
    fn write(&mut self, s: &str, id: u64, ord: u64)
             -> Result<usize, TermError> {

        let mut flags = 0u16;
        let buf = &mut self.to_buffer_mut();
        let size = Term::size_for(s, id, ord);
        let padded_size = if size & 1 == 1 { size + 1 } else { size };

        /* Sanity check */
        if buf.len() < padded_size {
            return Err(TermError::new("Target buffer is too small"));
        }

        /* Sanity check */
        if s.len() > 0xffff {
            return Err(TermError::new("Source string is too long"));
        }

        /* Copy string */
        let mut p: usize = s.len();
        buf[..p].copy_from_slice(s.as_bytes());

        /* Pad string to even boundary */
        if size & 1 == 1 {
            buf[p] = 0xff; p += 1;
            flags |= TermFlags::Padded as u16;
        }

        /* Copy identifier */
        let isz = Varint::write(&mut buf[p..], id);
        p += isz as usize;

        /* Copy ordinal */
        let osz = Varint::write(&mut buf[p..], ord);
        p += osz as usize;

        /* Fix size */
        self.constrain_size(p + TermField::VariableBegin as usize - 1);

        /* Write attrs, flags, string size */
        flags |= isz.to_flags() | (osz.to_flags() << 2);
        self.write_u8(TermField::Attributes, 0);
        self.write_u16(TermField::Flags, flags);
        self.write_u16(TermField::Strlen, s.len() as u16);
        self.write_u8(TermField::Version, 1);

        Ok(p)
    }
}

/// Constructors
impl<'t> Term<'t> {

    pub fn new(b: &'t mut [u8], s: &str, id: u64, ord: u64)
               -> Result<TermMut<'t>, TermError> {

        let len = Term::size_for(s, id, ord);

        debug_assert!(len <= b.len());
        for i in 0..len { b[i] = 0; } /* Zero buffer */

        let mut rv = TermMut {
            buffer: b, actual_size: len, expected_size: len
        };

        if let Err(e) = rv.write(&s, id, ord) {
            return Err(e);
        }

        Ok(rv)
    }

    pub fn from_bytes(b: &'t [u8]) -> Result<Term<'t>, TermError> {
        let l = b.len();
        let mut rv = Term { buffer: b, actual_size: l, expected_size: l };
        match Term::validate(&mut rv) {
            Err(e) => Err(e),
            Ok(s) => { rv.expected_size = s; Ok(rv) }
        }
    }

    pub fn from_bytes_mut(b: &'t mut [u8]) -> Result<TermMut<'t>, TermError> {
        let l = b.len();
        let mut rv = TermMut { buffer: b, actual_size: l, expected_size: l };
        match TermMut::validate(&mut rv) {
            Err(e) => Err(e),
            Ok(s) => { rv.expected_size = s; Ok(rv) }
        }
    }

    fn size_for(s: &str, id: u64, ord: u64) -> usize {
        Term::calculate_size(
          s.len(), Varint::from_u64(id), Varint::from_u64(ord)
        )
    }

    fn calculate_size(slen: usize, isz: Varint, osz: Varint) -> usize {
        slen + isz as usize + osz as usize
          + TermField::VariableBegin as usize - 1
    }
}

#[cfg(test)]
mod unit {
    use super::*;

    #[test]
    fn test_zeroes_ro() {
        let a: Vec::<u8> = vec![0; 16384];
        let b: Vec::<u8> = vec![0; TermField::MinSize as usize];
        let c: Vec::<u8> = vec![0; TermField::VariableBegin as usize];

        let x = Term::from_bytes(&a);
        let y = Term::from_bytes(&b);
        let z = Term::from_bytes(&c);

        assert!(x.is_ok());
        assert!(y.is_ok());
        assert!(z.is_err());
    }

    #[test]
    fn test_zeroes_rw() {
        let mut a: Vec::<u8> = vec![0; 16384];
        let mut b: Vec::<u8> = vec![0; TermField::MinSize as usize];
        let mut c: Vec::<u8> = vec![0; TermField::VariableBegin as usize];

        let x = Term::from_bytes(&mut a);
        let y = Term::from_bytes_mut(&mut b);
        let z = Term::from_bytes_mut(&mut c);

        assert!(x.is_ok());
        assert!(y.is_ok());
        assert!(z.is_err());

        let mut o = y.unwrap();
        assert!(o.write("", 42, 43).is_ok());

        assert_eq!(o.size(), 8);
        assert_eq!(o.strlen(), 0);
        assert_eq!(o.string(), "");
        assert_eq!(o.ordinal(), 43);
        assert_eq!(o.identifier(), 42);

    }

    #[test]
    fn test_soak_valid() {
        let mut b = vec![0u8; 1024];
        let mut s = String::from("");
        let numbers = [1<<8-1, 1<<16-1, 1<<32-1, 1<<64-1];

        for _ in 0..=1001 {
            for i in &numbers {
                for o in &numbers {
                    let t1 = Term::new(&mut b, &s, *i, *o).unwrap();
                    let size = t1.size();
                    let t2 = Term::from_bytes(&b[..size]).unwrap();
                    assert_eq!(t2.string(), s);
                    assert_eq!(t2.ordinal(), *o);
                    assert_eq!(t2.identifier(), *i);
                }
            }
            s.push('*');
        }
    }

    #[test]
    fn test_expandable() {
        let mut b = vec![0u8; 10];

        let t1 = Term::new(&mut b, "A", 254, 255).unwrap();
        assert_eq!(t1.string(), "A");
        assert_eq!(t1.ordinal(), 255);
        assert_eq!(t1.identifier(), 254);

        let len = t1.strlen();
        drop(t1);

        let mut v = b[0..=len].to_vec();
        v.resize(1024, 0xff);
        v.extend_from_slice(&b[len..]);

        let t2 = Term::from_bytes(v.as_slice()).unwrap();
        assert_eq!(t2.string(), "A");
        assert_eq!(t2.ordinal(), 255);
        assert_eq!(t2.identifier(), 254);
        assert!(t2.size() > t2.expected_size());
    }

    #[test]
    fn test_validity() {
        let a = vec![];
        let b = vec![0; 7];
        let c = vec![0x55; 64];
        let mut d = vec![0, 0, 0, 0, 0, 0, 0, 1];

        let t1 = Term::from_bytes(&a);
        let t2 = Term::from_bytes(&b);
        let t3 = Term::from_bytes(&c);
        let t4 = Term::from_bytes_mut(&mut d);

        assert!(t1.is_err());
        assert!(t2.is_err());
        assert!(t3.is_err());
        assert!(t4.is_ok());

        t4.unwrap().write_u16(TermField::Strlen, 1);

        let t5 = Term::from_bytes(&d);
        assert!(t5.is_err());
    }
}

